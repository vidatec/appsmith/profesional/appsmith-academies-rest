export default {
	openLessonModal: (id) => {
		storeValue('courseId', id);
		return showModal('coursesLessonsModal');
	}
}