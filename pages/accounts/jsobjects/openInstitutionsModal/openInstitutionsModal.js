export default {
	openInstitutionsModal: (id) => {
		storeValue('accountId', id);
		return showModal('InstitutionsModal');
	}
}