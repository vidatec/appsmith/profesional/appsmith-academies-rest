export default {
	openProgressModal: (id) => {
		storeValue('enrollmentId', id);
		return showModal('progressesModal');
	}
}